﻿using UnityEngine;
using UnityEngine.Serialization;


[CreateAssetMenu(fileName = "FeatureCSO", order = 0)]
public class FeatureCSO : ScriptableObject
{
    [FormerlySerializedAs("HD")] public int SD;
    public bool flag;
}